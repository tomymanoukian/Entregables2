#include <stdio.h>

#ifndef MATRIZ__H
#define MATRIZ__H

#define MAX_COL 1000

typedef enum tipo_matriz {positiva, no_positiva, negativa, no_negativa, no_cumple} positividad;

int traza(int m[][MAX_COL], size_t fil, size_t col); /*calcula la traza de una matriz*/

void suma_numero_matriz(int m[][MAX_COL], size_t fil, size_t col, int numero); /*suma un numero entrero a todos los componentes de una matriz*/

void multiplica_numero_matriz(int m[][MAX_COL], size_t fil, size_t col, int numero); /*multiplica un numero entrero a todos los componentes de una matriz*/

void transpuesta(int a[][MAX_COL], size_t fil_a, size_t col_a, int b[][MAX_COL], size_t fil_b, size_t col_b); /*se ingresan dos matrices y a la segunda le carga la traspuesta de la primera*/

positividad positividad_matriz (int m[][MAX_COL], size_t fil, size_t col); /*se ingresa una matriz y retorna si es positiva, negativa, etc.*/

int determinante_2x2_3x3 (int m[][MAX_COL], size_t fil, size_t col); /*calcula el determinante de matrices de 2x2 y 3x3*/

int elemento_max (int m[][MAX_COL], size_t fil, size_t col); /*retorna el elemento maximo de una matriz*/

int norma_1 (int m[][MAX_COL], size_t fil, size_t col); /*retorna el maximo de la suma, en valores absolutos, de los elementos de cada columna de una matriz*/

int norma_infinito (int m[][MAX_COL], size_t fil, size_t col); /*retorna el maximo de la suma, en valores absolutos, de los elementos de cada fila de una matriz*/

#endif