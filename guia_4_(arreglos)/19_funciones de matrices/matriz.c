#include <stdio.h>
#include "matriz.h"

int traza(int m[][MAX_COL], size_t fil, size_t col) {
	int i, resultado_traza;
	resultado_traza = 0;

    if(fil != col) /*si la matriz no es cuadrada retorna un 0*/
	    return 0;

    for(i = 0; i < col; i++){       /*suma los elementos de la diagonal y los retorna*/
        resultado_traza += m[i][i];
    }

    return resultado_traza;
}

void suma_numero_matriz(int m[][MAX_COL], size_t fil, size_t col, int numero) {
	int i, j;

    for(i = 0; i < fil; i++) {
        for(j = 0; j < col; j++)
        	m[i][j] += numero;
    }
}

void multiplica_numero_matriz(int m[][MAX_COL], size_t fil, size_t col, int numero) {
	int i, j;

    for(i = 0; i < fil; i++) {
        for(j = 0; j < col; j++)
        	m[i][j] *= numero;
    }
}

void transpuesta(int a[][MAX_COL], size_t fil_a, size_t col_a, int b[][MAX_COL], size_t fil_b, size_t col_b) {
	int i, j;

	if (fil_a == col_b && col_a == fil_b) { /*comprueba que se cumplan las condiciones necesarias para poder transponer la matriz*/
        for(i = 0; i < fil_a; i++) {
            for(j = 0; j < col_a; j++)
                b[j][i] = a[i][j];
        }
        	
    }
}

positividad positividad_matriz (int m[][MAX_COL], size_t fil, size_t col) {
    int i, j, negativo, positivo, nulo, cant_elementos;

    negativo = positivo = nulo = 0;
    cant_elementos = fil * col;

    for(i = 0; i < fil; i++) {
        for(j = 0; j < col; j++) {

            if(m[i][j] < 0)
                negativo++;
            else if(m[i][j] > 0)
                positivo++;
            else if(m[i][j] == 0)
            	nulo++;            
        }
    }

    if(cant_elementos == positivo)
    	return positiva;
    else if(cant_elementos == negativo)
    	return negativa;
    else if(cant_elementos == positivo + nulo)
    	return no_negativa;
    else if(cant_elementos == negativo + nulo)
    	return no_positiva;
    else
    	return no_cumple;
}

int determinante_2x2_3x3 (int m[][MAX_COL], size_t fil, size_t col) {
    int det_2x2, det_3x3;

    if (fil == col && fil == 2) { /*si se trata de una matriz de 2x2*/

        det_2x2 = m[0][0] * m[1][1] - m[1][0] * m[0][1];
        return det_2x2;
    }

    else if (fil == col && fil == 3) { /*si se trata de una matriz de 3x3*/

        det_3x3 = m[0][0] * m[1][1] * m[2][2] + m[0][1] * m[1][2] * m[2][0] + m[0][2] * m[2][1] * m[1][0] - m[0][2] * m[1][1] * m[2][0] - m[0][0] * m[1][2] * m[2][1] - m[0][1] * m[2][2] * m[1][0];
        return det_3x3;
    }

    else {
    	return 0; /*en cualquier otro caso*/
    }
}

int elemento_max (int m[][MAX_COL], size_t fil, size_t col) {
    int elemento_max, i, j;
    elemento_max = m[0][0];

    for(i = 0; i < fil; i++) {
        for(j = 1; j < col; j++){

            if(m[i][j] > elemento_max)
                elemento_max = m[i][j];
        }
    }

    return elemento_max;
}

int norma_1 (int m[][MAX_COL], size_t fil, size_t col) {
	int i, j, suma_col, norma_max;
	norma_max = 0;

    for(j = 0; j < col; j++) {
        suma_col = 0;

        for(i = 0; i < fil; i++)
            suma_col += m[i][j];

        if(suma_col < 0)
            suma_col *= (-1); 

        if(suma_col > norma_max)
            norma_max = suma_col;
    }

    return norma_max;   
}

int norma_infinito (int m[][MAX_COL], size_t fil, size_t col) {
    int i, j, suma_fil, norma_max;
    norma_max = 0;

    for(i = 0; i < fil; i++) {
        suma_fil = 0;

        for(j = 0; j < col; j++)
            suma_fil += m[i][j];

        if(suma_fil < 0)
            suma_fil *= (-1);

        if(suma_fil > norma_max)
            norma_max = suma_fil;
    }

    return norma_max;
}