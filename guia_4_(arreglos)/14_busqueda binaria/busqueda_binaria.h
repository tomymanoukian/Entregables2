#ifndef BUSQUEDA_BINARIA__H
#define BUSQUEDA_BINARIA__H

int busqueda_binaria(int v[], size_t n, int objetivo); /*recibe un vector ordenado y ubica la posición de uno de los elementos*/

#endif