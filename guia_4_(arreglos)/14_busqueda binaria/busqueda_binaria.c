#include <stdio.h>
#include "busqueda_binaria.h"

int busqueda_binaria(int v[], size_t n, int objetivo) {
	
	int izq, der, pivot, i;
	izq = i = 0;
    der = n;
    
    do
	{
		pivot = ((der - izq) / 2 ) + izq;

		if(v[pivot] > objetivo)
			der = pivot;

		if(v[pivot] < objetivo)
			izq = pivot;

		i++;
		if(i == n / 2 + 1)
			return -1;

	} while(v[pivot] != objetivo);

	return pivot;
}