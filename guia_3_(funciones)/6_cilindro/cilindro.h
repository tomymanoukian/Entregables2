#ifndef CILINDRO__H
#define CILINDRO__H
#endif

#include <math.h>

#ifndef M_PI
#define M_PI 3.14159

double cilindro_diametro(double radio); /*retorna el diametro de un cilindro*/

double cilindro_circunsferencia(double radio); /*retorna la circunsferencia de un cilindro*/

double cilindro_base(double radio); /*retorna el area de la base de un cilindro*/

double cilindro_volumen(double altura, double radio); /*retorna el volumen de un cilindro*/
#endif

