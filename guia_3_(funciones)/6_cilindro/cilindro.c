#include <stdio.h>
#include <math.h>
#include "cilindro.h"

double cilindro_diametro(double radio) {
	if (radio < 0)
	    return -1;
    else
        return radio * 2;
}

double cilindro_circunsferencia(double radio) {
	if (radio < 0)
	    return -1;
    else
        return radio * M_PI;
    
}

double cilindro_base(double radio) {
   	if (radio < 0)
	    return -1;
    else
        return radio * radio * M_PI;

}

double cilindro_volumen(double altura, double radio) {
		if (altura < 0 || radio < 0)
	    return -1;
    else
        return radio * radio * altura * M_PI;

}