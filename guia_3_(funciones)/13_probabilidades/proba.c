#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "proba.h"


int proba_1(void) {

    float random;
    srand(time(NULL));
    random =(float) rand() / RAND_MAX;

    if(random <= LIM_PROBA_1)
        return 0;
    else
    	return 1;
}

int proba_2(void) {

    float random;
    srand(time(NULL));
    random =(float) rand() / RAND_MAX;

    if(random <= LIM_1_PROBA_2)
    	return 3;

    if(random > LIM_1_PROBA_2 && random <= LIM_2_PROBA_2)
        return 9;

    else
    	return 5;
}

void proba_moneda (void) {

    float random;
    srand(time(NULL));
    random =(float) rand() / RAND_MAX;

    if(random < PROBA_CARA_MONEDA)
        printf("Cara\n");

    else
    	printf("Seca\n");
}

void proba_dado (void) {
    
    cara_dado cara;

    float random;
    srand(time(NULL));
    random =(float) rand() / RAND_MAX;

    if (random <= PROBA_CARA_DADO)
    	cara = I;

    else if (random > PROBA_CARA_DADO && random <= PROBA_CARA_DADO * 2)
    	cara = II;

    else if (random > PROBA_CARA_DADO * 2 && random <= PROBA_CARA_DADO * 3)
    	cara = III;

    else if (random > PROBA_CARA_DADO * 3 && random <= PROBA_CARA_DADO * 4)
    	cara = IV;

    else if (random > PROBA_CARA_DADO * 4 && random <= PROBA_CARA_DADO * 5)
    	cara = V;

    else
    	cara = VI;

    dado (cara);
}

void dado (cara_dado cara) {
    
    switch (cara) {

        case I:
        printf("|     |\n|  *  |\n|     |\n");
        break;

        case II:
        printf("|    *|\n|     |\n|*    |\n");
        break;

        case III:
        printf("|*    |\n|  *  |\n|    *|\n");
        break;

        case IV:
        printf("|*   *|\n|     |\n|*   *|\n");
        break;

        case V:
        printf("|*   *|\n|  *  |\n|*   *|\n");
        break;

        case VI:
        printf("|*   *|\n|*   *|\n|*   *|\n");

    }
}