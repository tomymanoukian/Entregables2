#ifndef PROBA__H
#define PROBA__H

#define LIM_PROBA_1 0.2

#define LIM_1_PROBA_2 0.1
#define LIM_2_PROBA_2 0.45

#define PROBA_CARA_MONEDA 0.5

#define PROBA_CARA_DADO 1.0 / 6.0

typedef enum caras {I, II, III, IV, V, VI} cara_dado;

int proba_1(void); /*imprime 0 con un 20% de probabilidad y 1 con un 80% de probabilidad*/

int proba_2(void); /*imprime 3 con un 10% de probabilidad, 9 con un 35% de probabilidad y 5 con un 55% de probabilidad*/

void proba_moneda (void); /*imprime cara o seca con un 50% de probabilidad cada una*/

void proba_dado (void); /*imprime las caras de un dado con un 1/6 de probabilidades cada una*/

void dado (cara_dado cara); /*funcion auxiliar para imprimir graficamente las caras de un dado*/

#endif