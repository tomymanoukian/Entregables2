#include <stdio.h>
#include <stdlib.h>

void funcion (void) {
    static int x = 0; /*se define una variable local estática, esta conservará su valor luego de salir de la función 
                         para que pueda ser utilizada nuevamente en caso de que se vuelva a llamar a la función*/
    x++;
    printf("%d\n", x); /*imprime el valor de x*/
}

int manin (void)
{
    funcion(); /*se aumenta el valor de  x en 1 (x = 0 >> x = 1) y se lo imprime*/
    funcion(); /*se aumenta el valor de  x en 1 (x = 1 >> x = 2) y se lo imprime*/
    funcion(); /*se aumenta el valor de  x en 1 (x = 2 >> x = 3) y se lo imprime*/
    funcion(); /*se aumenta el valor de  x en 1 (x = 3 >> x = 4) y se lo imprime*/
    funcion(); /*se aumenta el valor de  x en 1 (x = 4 >> x = 5) y se lo imprime*/

    return EXIT_SUCCESS;
}