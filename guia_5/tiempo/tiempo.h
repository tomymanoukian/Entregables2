#ifndef TIEMPO__H
#define TIEMPO__H

#include <stdio.h>

typedef enum {ST_OK, ST_ERROR_SEG_NEGATIVO} status_t;

status_t seg_a_hs_min_seg(int segundos, int *hs, int *min, int *seg); /*se ingresa una cantidad de segundos y retorna la cantidad de hs, min, seg por la interfaz*/

#endif