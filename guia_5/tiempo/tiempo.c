#include <stdio.h>
#include "tiempo.h"

status_t seg_a_hs_min_seg(int seg_tot, int *hs, int *min, int *seg) {
    
    int horas, minutos, segundos;

    if(seg_tot < 0)
        return ST_ERROR_SEG_NEGATIVO;

    horas = 0;
    for(; seg_tot >= 3600; seg_tot -= 3600) {
        horas++;
    }
	
    minutos = 0;
    for(; seg_tot >= 60; seg_tot -= 60) {
        minutos++;
    }

	*hs = horas;
    *min = minutos;
    *seg = seg_tot;

    return ST_OK;
}