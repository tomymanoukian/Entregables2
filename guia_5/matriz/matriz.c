#include <stdio.h>
#include "matriz.h"

int main (void)
{
    int matriz[FILAS][COLUMNAS] = {{1, 2, 3, 4}, {5, 6, 7, 8,}, {9, 10, 11, 12}};
    int *ptr;
    int(*ptr2vector)[COLUMNAS];
    int fila, col, i;

    /*primera forma:*/

    ptr = (int *)matriz; /*guardo la matriz en ptr luego de castearla a puntero*/
    for(i = 0; i < COLUMNAS * FILAS; i++)
        printf("%i\n", *(ptr + i)); /*recorro el vector donde guardé la matriz y lo imprimo*/

    /*segunda forma:*/

    ptr2vector = matriz; /*guardo la matriz en un vector de punteros*/
    for(fila = 0; fila < FILAS; fila++) {
        
        for(col = 0; col < COLUMNAS; col++) {
            printf("%d\n", *(*(ptr2vector + fila) + col));
        } /*recorro los elementos de cada fila de la matriz y los imprimo*/
    }

    return 0;
}