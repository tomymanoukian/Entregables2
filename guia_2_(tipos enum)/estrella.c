#include <stdio.h>
#include "estrella.h"

int main(void)
{
    double temp;
    espectral_t clase;

    printf("Ingrese la temperatura del cuerpo\n");
    scanf("%lf",& temp);

    if(temp > TEMP_O_MIN && temp < TEMP_O_MAX)
        clase = clase_O;
    else  if(temp > TEMP_B_MIN && temp < TEMP_B_MAX)
    	clase = clase_B;
    else if(temp > TEMP_A_MIN && temp < TEMP_A_MAX)
    	clase = clase_A;
    else if(temp > TEMP_F_MIN && temp < TEMP_F_MAX)
    	clase = clase_F;
    else if(temp > TEMP_G_MIN && temp < TEMP_F_MAX)
    	clase = clase_G;
    else if(temp > TEMP_K_MIN && temp < TEMP_G_MAX)
    	clase = clase_K;
    else if(temp > TEMP_M_MIN && temp < TEMP_M_MAX)
    	clase = clase_M;
    else
    	printf("temperatura inválida\n");

    switch (clase) {
        case clase_O:
        printf("azul\n");
        break;

        case clase_B:
        printf("blanco azulado\n");
        break;

        case clase_A:
        printf("blanco\n");
        break;

        case clase_F:
        printf("blanco amarillento\n");
        break;

        case clase_G:
        printf("amarillo\n");
        break;

        case clase_K:
        printf("naranja\n");
        break;

        case clase_M:
        printf("rojo\n");
        break;

        default:
        printf("error\n");

    }

    return 0;

}