#ifndef ESTRELLA__H
#define ESTRELLA__H

typedef enum clase {clase_O, clase_B, clase_A, clase_F, clase_G, clase_K, clase_M} espectral_t;

#define TEMP_O_MIN 28000
#define TEMP_O_MAX 50000
#define TEMP_B_MIN 9600
#define TEMP_B_MAX 28000
#define TEMP_A_MIN 7100
#define TEMP_A_MAX 9600
#define TEMP_F_MIN 5700
#define TEMP_F_MAX 7100
#define TEMP_G_MIN 4600
#define TEMP_G_MAX 5700
#define TEMP_K_MIN 3200
#define TEMP_K_MAX 4600
#define TEMP_M_MIN 1700
#define TEMP_M_MAX 3200
#endif
